(* ::Package:: *)

BeginPackage["waveguide`"]


ResonatorDecayRate::usage = "Returns the rate at which photons decay out of the cavity"
SidebandExcitationRate::usage = "Returns the rate at which the molecule is excited on the red sideband"
SpontaneousDecayRate::usage = "Returns the rate of spontaneous emission from the excited molecule"
CoolingRate::usage = "Returns the rate at which the vibrational state quantum number decreases"


Begin["`Private`"];


ResonatorDecayRate[\[Omega]_, Q_]:=\[Omega]/Q


SidebandExcitationRate[\[Eta]_, Rabi_, \[Gamma]sp_]:=(\[Eta] Rabi)^2/\[Gamma]sp
SpontaneousDecayRate[vacuumRabi_, \[Omega]_, Q_]:= Module[{g, \[Kappa]},
g = vacuumRabi;
\[Kappa] = ResonatorDecayRate[\[Omega], Q];
(2g^2)/\[Kappa]
]


CoolingRate[spontDecayRate_, excitationRate_]:=Module[{\[Gamma]sp, R},
\[Gamma]sp= spontDecayRate;
R = excitationRate;
(\[Gamma]sp R)/(2R+\[Gamma]sp)
]

CoolingRate[Rabi_, vacuumRabi_,\[Eta]_, \[Omega]_, Q_]:=Module[{\[Gamma]sp, R, \[Kappa]},
\[Gamma]sp= SpontaneousDecayRate[vacuumRabi, \[Omega], Q];
R = SidebandExcitationRate[\[Eta], Rabi];
CoolingRate[\[Gamma]sp,R]
]


End[]
EndPackage[]




